﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoClienteSw.Model
{
    public class Warehouse
    {
        public int id { get; set; }
        public String name { get; set; }

        public String address { get; set; }
    }
}
