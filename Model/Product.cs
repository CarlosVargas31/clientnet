﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProyectoClienteSw.Model
{
    public class Product
    {
        public int code { get; set; }
        public string name { get; set; }
        public double  price { get; set; }

    }
}
