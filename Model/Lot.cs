﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProyectoClienteSw.Model
{
    public  class Lot
    {
        public long id { get; set; }

        public Product product { get; set; }
        public Warehouse warehouse { get; set; }

        public int stock { get; set; }
    }
}
