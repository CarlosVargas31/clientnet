﻿using ProyectoClienteSw.JsonControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI.WarehouseGUI
{
    public partial class GUIWarehouse : Form
    {
        public GUIWarehouse()
        {
            InitializeComponent();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();


            try
            {
                JsonController jsoncontroller = new JsonController();
                dynamic result = jsoncontroller.Get(jsoncontroller.getLocalhost() +
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/serviceREST/getAllWarehouses");

                dataGridView1.DataSource = result;

            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
}
