﻿using ProyectoClienteSw.JsonControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI
{
    public partial class GUIBuscarProducto : Form
    {
        public GUIBuscarProducto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            try
            {
                JsonController jsoncontroller = new JsonController();
                dynamic result = jsoncontroller.Get(jsoncontroller.getLocalhost() + "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Products/findProduct?code=" +
                    Convert.ToInt32(txtCodigoBuscado.Text));


                txtCodigo.Text = result.code;
                txtNombre.Text = result.name;
                txtPrecio.Text = result.price;

                String titulo = " INFORMATION ";
                MessageBox.Show("Buscado  Correctamente ", titulo);
                
            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
}
