﻿using Newtonsoft.Json;
using ProyectoClienteSw.JsonControllers;
using ProyectoClienteSw.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI
{
    public partial class GUICrearProducto : Form
    {
        public GUICrearProducto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {


            
            int codigo =Convert.ToInt32( txtCodigo.Text);
            String nombre = txtNombre.Text;
            double precio = Convert.ToDouble(txtPrecio.Text);
            

            Product product = new Product { code = codigo, name = nombre, price = precio };
            
            

            try
            {
                
                JsonController jsoncontroller = new JsonController();
                string json = JsonConvert.SerializeObject(product);

                dynamic result = jsoncontroller.Post(jsoncontroller.getLocalhost() +
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Products/saveProduct", json);
                String titulo = " INFORMATION ";
                MessageBox.Show("Creado correctamente " , titulo);

                txtCodigo.Text = "";
                txtNombre.Text = "";
                txtPrecio.Text = "";
            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error "+ ex, titulo);

            }



        }
    }
}
