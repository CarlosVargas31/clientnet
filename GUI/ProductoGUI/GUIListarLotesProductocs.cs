﻿using ProyectoClienteSw.JsonControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI.ProductoGUI
{
    public partial class GUIListarLotesProductocs : Form
    {
        public GUIListarLotesProductocs()
        {
            InitializeComponent();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();
            JsonController jsoncontroller = new JsonController();
            dynamic result = jsoncontroller.Get(jsoncontroller.getLocalhost() +
                "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Products/findLotsProduct?code=" + 
                Convert.ToInt32( txtParametro.Text));

            dataGridView1.DataSource = result;
        }
    }
}
