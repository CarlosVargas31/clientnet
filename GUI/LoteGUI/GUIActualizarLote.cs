﻿using Newtonsoft.Json;
using ProyectoClienteSw.JsonControllers;
using ProyectoClienteSw.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI.LoteGUI
{
    public partial class GUIActualizarLote : Form
    {
        public GUIActualizarLote()
        {
            InitializeComponent();
        }

        int codeProduct = 0;
        int idWarehouse = 0;

        private void button1_Click(object sender, EventArgs e)
        {
            

            int code = Convert.ToInt32(txtCodigoBuscado.Text);

            try
            {
                JsonController jsoncontroller = new JsonController();
                dynamic result = jsoncontroller.Get(jsoncontroller.getLocalhost() +
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Lots/find?id=" +
                   code);


                idWarehouse = result.warehouse.id;
                codeProduct = result.product.code;


                txtIdInterno.Text = result.id;
                txtAlmacen.Text = result.warehouse.name;
                txtProducto.Text = result.product.name;
                txtStock.Text = result.stock;

                String titulo = " INFORMATION ";
                MessageBox.Show("Buscado  Correctamente ", titulo);
                button2.Enabled = true;
            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int code = Convert.ToInt32(txtCodigoBuscado.Text);
           
            try
            {
                JsonController jsoncontroller = new JsonController();
               
                dynamic resultProduct = jsoncontroller.Get(jsoncontroller.getLocalhost() +
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Products/findProduct?code=" +
                    codeProduct);

                dynamic resultWarehouse = jsoncontroller.Get(jsoncontroller.getLocalhost() +
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/serviceREST/findWarehouse?id=" +
                    idWarehouse);

                Product productFind = new Product
                {
                    code = resultProduct.code,
                    name = resultProduct.name,
                    price = resultProduct.price
                };
                Warehouse warehouseFind = new Warehouse
                {
                    id = resultWarehouse.id,
                    name = resultWarehouse.name,
                    address = resultWarehouse.address
                };

                Lot lot = new Lot
                {
                    id = code,
                    product = productFind,
                    warehouse = warehouseFind,
                    stock = Convert.ToInt32(txtStock.Text)

                };



                jsoncontroller.Delete(jsoncontroller.getLocalhost() +
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Lots/remove?id=" +
                   Convert.ToInt32(txtCodigoBuscado.Text));
                
                string json = JsonConvert.SerializeObject(lot);

                dynamic result2 = jsoncontroller.Post(jsoncontroller.getLocalhost() +
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Lots/save?code=" + codeProduct + "&id=" + idWarehouse
                    , json);




                String titulo = " INFORMATION ";
                MessageBox.Show("Actualizado  Correctamente ", titulo);

                txtIdInterno.Text = "";
                txtProducto.Text = "";
                txtAlmacen.Text = "";
                txtStock.Text = "";
            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
}
