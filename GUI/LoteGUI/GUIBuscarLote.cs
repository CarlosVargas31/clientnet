﻿using ProyectoClienteSw.JsonControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI.LoteGUI
{
    public partial class GUIBuscarLote : Form
    {
        public GUIBuscarLote()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            int code = Convert.ToInt32(txtCodigoBuscado.Text);

            try
            {
                JsonController jsoncontroller = new JsonController();
                dynamic result = jsoncontroller.Get(jsoncontroller.getLocalhost() +
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Lots/find?id=" +
                   code);

                txtIdInterno.Text = result.id;
                txtAlmacen.Text = result.warehouse.name;
                txtProducto.Text = result.product.name;
                txtStock.Text = result.stock;

                String titulo = " INFORMATION ";
                MessageBox.Show("Buscado  Correctamente ", titulo);
             
            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
}
