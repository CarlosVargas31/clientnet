﻿using ProyectoClienteSw.JsonControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI.LoteGUI
{
    public partial class GUIEliminarLote : Form
    {
        public GUIEliminarLote()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            try
            {
                JsonController jsoncontroller = new JsonController();
                jsoncontroller.Delete(jsoncontroller.getLocalhost() +
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Lots/remove?id=" +
                   Convert.ToInt32(txtCodigoBuscado.Text));

                String titulo = " INFORMATION ";
                MessageBox.Show("Eliminado  Correctamente ", titulo);

                txtIdInterno.Text = "";
                txtProducto.Text = "";
                txtAlmacen.Text = "";
                txtStock.Text = "";
            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            

            int code = Convert.ToInt32(txtCodigoBuscado.Text);

            try
            {
                JsonController jsoncontroller = new JsonController();
                dynamic result = jsoncontroller.Get(jsoncontroller.getLocalhost() +
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Lots/find?id=" +
                   code);

                txtIdInterno.Text = result.id;
                txtAlmacen.Text = result.warehouse.name;
                txtProducto.Text = result.product.name;
                txtStock.Text = result.stock;

                String titulo = " INFORMATION ";
                MessageBox.Show("Buscado  Correctamente ", titulo);
                button2.Enabled = true;
            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
}
