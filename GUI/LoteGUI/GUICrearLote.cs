﻿using Newtonsoft.Json;
using ProyectoClienteSw.JsonControllers;
using ProyectoClienteSw.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI.LoteGUI
{
    public partial class GUICrearLote : Form
    {
        public GUICrearLote()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {




            long codigo = Convert.ToInt64(txtIdInterno.Text);
            int codeWarehouse = Convert.ToInt32(txtAlmacen.Text);
            int codeProduct = Convert.ToInt32(txtProducto.Text);
            int cantidad = Convert.ToInt32(txtStock.Text);

            


            try
            {
                JsonController jsoncontroller = new JsonController();
                dynamic resultProduct = jsoncontroller.Get(jsoncontroller.getLocalhost() + 
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Products/findProduct?code=" +
                    codeProduct);

                dynamic resultWarehouse = jsoncontroller.Get(jsoncontroller.getLocalhost() + 
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/serviceREST/findWarehouse?id=" +
                    codeWarehouse);

                Product productFind = new Product
                {
                    code = resultProduct.code,
                    name = resultProduct.name,
                    price = resultProduct.price
                };
                Warehouse warehouseFind= new Warehouse
                {
                    id= resultWarehouse.id,
                    name= resultWarehouse.name,
                    address= resultWarehouse.address
                };

                Lot lot = new Lot
                {
                    id = codigo,
                    product = productFind,
                    warehouse =warehouseFind,
                    stock=cantidad

                };
                
                string json = JsonConvert.SerializeObject(lot);

                dynamic result = jsoncontroller.Post(jsoncontroller.getLocalhost() +
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Lots/save?code="+codeProduct+"&id="+codeWarehouse
                    , json);



                String titulo = " INFORMATION ";
                MessageBox.Show("Adicionado Correctamente ", titulo);

                txtAlmacen.Text = "";
                txtIdInterno.Text = "";
                txtProducto.Text = "";
                txtIdInterno.Text = "";
                txtStock.Text = "";

            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
}
