﻿using ProyectoClienteSw.JsonControllers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoClienteSw.GUI.LoteGUI
{
    public partial class GUIListarPorParametroLote : Form
    {
        public GUIListarPorParametroLote()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.DataSource = null;
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();

            


            try
            {

                JsonController jsoncontroller = new JsonController();
                dynamic result = jsoncontroller.Get(jsoncontroller.getLocalhost() +
                    "/ServerStoreREST-ProjectServerStoreREST-context-root/resources/api/v1/Lots/findByStock?stock=" +
                    Convert.ToInt32(txtCodigoBuscado.Text));

                dataGridView1.DataSource = result;
                String titulo = " INFORMATION ";
                MessageBox.Show("Listado Correctamente ", titulo);


            }
            catch (Exception ex)
            {
                String titulo = " INFORMATION ";
                MessageBox.Show("Error " + ex, titulo);

            }
        }
    }
    
}
